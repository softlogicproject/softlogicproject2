/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pattarapon
 */
public class Customer {
    private int id;
    private String name;
    private String tel;
    private String gender;
    private String address;
    private int point;

    public Customer(int id, String name, String tel, String gender, String address, int point) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.gender = gender;
        this.address = address;
        this.point = point;
    }
    public Customer(String name, String tel, String gender, String address, int point) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.gender = gender;
        this.address = address;
        this.point = point;
    }
    public Customer() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        this.gender = "";
        this.address = "";
        this.point = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    

    
    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setName(rs.getString("customer_name"));
            customer.setTel(rs.getString("customer_tel"));
            customer.setGender(rs.getString("customer_gender"));
            customer.setAddress(rs.getString("customer_address"));
            customer.setPoint(rs.getInt("customer_point"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }
}

